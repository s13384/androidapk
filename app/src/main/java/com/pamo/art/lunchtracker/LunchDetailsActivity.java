package com.pamo.art.lunchtracker;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.app.AlertDialog;
import android.widget.Toast;

public class LunchDetailsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_lunch_details);

        final Database db = new Database(getApplicationContext());
        final int id = getIntent().getExtras().getInt("id");
        final Lunch lunch = db.getLunchWithId(id);

        TextView nameTV = (TextView) findViewById(R.id.lunchDetail_name_textView);
        TextView dateTV = (TextView) findViewById(R.id.lunchDetail_date_textView);
        TextView caloriesTV = (TextView) findViewById(R.id.lunchDetail_calories_textView);
        nameTV.setText(lunch.getName());
        dateTV.setText(lunch.getDate());
        caloriesTV.setText("calories");
        new Calories(caloriesTV).execute(lunch.getName());
        Button deleteLunchBtn = (Button) findViewById(R.id.delete_lunch_button);
        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        builder.setTitle("Confirm");
        builder.setMessage("Are you sure?");
        builder.setPositiveButton("YES", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                // Do nothing but close the dialog
                db.deleteLunch(id);
                Toast.makeText(getApplicationContext(), "Deleted " + lunch.getName(), Toast.LENGTH_LONG).show();
                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                startActivity(intent);
                dialog.dismiss();
            }
        });
        builder.setNegativeButton("NO", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                // Do nothing
                dialog.dismiss();
            }
        });
        final AlertDialog alert = builder.create();

        deleteLunchBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alert.show();
            }
        });
    }
}
