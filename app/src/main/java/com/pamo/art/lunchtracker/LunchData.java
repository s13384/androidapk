package com.pamo.art.lunchtracker;

import java.util.ArrayList;

/**
 * Created by Art on 2017-11-07.
 */

public class LunchData {
    private static LunchData instance;
    private static ArrayList<Lunch> lunches;

    public LunchData LunchData(Database db){
        this.LunchData();
        this.lunches = db.getAllLunches();
        return instance;
    }

    public LunchData LunchData(){
        if (instance == null)
            instance = new LunchData();
        lunches = new ArrayList<Lunch>();
        return instance;
    }

    public synchronized ArrayList<Lunch> getAllLunches(){
        return lunches;
    }

    public synchronized void addLunch(String name, String date){
        Lunch lu = new Lunch(0,name,date);
        this.lunches.add(lu);
    }

    public synchronized Lunch getLunchWithId(int id)
    {
        Lunch lu = null;
        for(Lunch tmpLu : lunches){
            if (tmpLu.getId() == id)
                lu = tmpLu;
        }
        return lu;
    }

    public synchronized void deleteLunch(int id){
        for(Lunch tmpLu : lunches){
            if (tmpLu.getId() == id)
                lunches.remove(tmpLu);
        }
    }

    public synchronized void assignData(ArrayList<Lunch> list){
        lunches = list;
    }
}
