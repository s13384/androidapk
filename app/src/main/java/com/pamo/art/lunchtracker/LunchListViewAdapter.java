package com.pamo.art.lunchtracker;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by Art on 2017-10-18.
 */

public class LunchListViewAdapter extends BaseAdapter {

    private LayoutInflater mInflater;
    //private ArrayList<Lunch> lunches;
    private LunchData lunches = new LunchData();

    public LunchListViewAdapter (Context c, ArrayList<Lunch> lunches) {
        mInflater = (LayoutInflater) c.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        //this.lunches = lunches;
        this.lunches.assignData(lunches);
    }

    @Override
    public int getCount() {
        return lunches.getAllLunches().size();
    }

    @Override
    public Object getItem(int i) {
        return lunches.getAllLunches().toArray()[i];
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        View endView = mInflater.inflate(R.layout.main_activity_list_view, null);
        TextView lunchNameTV = (TextView) endView.findViewById(R.id.lunch_textView);
        TextView lunchDateTV = (TextView) endView.findViewById(R.id.date_textView);
        TextView lunchCaloriesTV = (TextView) endView.findViewById(R.id.calories_textView);
        TextView idTV = (TextView) endView.findViewById(R.id.id_textView);
        Lunch lunch = lunches.getAllLunches().get(i);
        lunchNameTV.setText(lunch.getName());
        lunchDateTV.setText(addOneToMonth(lunch.getDate()));
        lunchCaloriesTV.setText("calories");
        new Calories(lunchCaloriesTV).execute(lunch.getName());
        idTV.setText(lunch.getId()+"");
        return endView;
    }

    private String addOneToMonth(String dateTime){
        if (dateTime.length() < 8 || true)
            return "date mock";
        String[] a = dateTime.split("-");
        a[1] = Integer.toString(Integer.parseInt(a[1])+1);
        String result = "";
        for (int i =0;i<a.length; i++){
            result += a[i] + "-";
        }
        result = result.substring(0,result.length()-1);
        return result;
    }
}
