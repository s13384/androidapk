package com.pamo.art.lunchtracker;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.pamo.art.lunchtracker.LunchListFragment.OnListFragmentInteractionListener;

import java.util.ArrayList;
import java.util.List;

/**
 * {@link RecyclerView.Adapter} that can display a {@link DummyItem} and makes a call to the
 * specified {@link OnListFragmentInteractionListener}.
 * TODO: Replace the implementation with code for your data type.
 */
public class LunchFragmentListViewAdapter extends RecyclerView.Adapter<LunchFragmentListViewAdapter.ViewHolder> {

    private final List<Lunch> mValues;
    private LunchData mLunches;
    private final OnListFragmentInteractionListener mListener;
    private Context context;

    public LunchFragmentListViewAdapter(Context c, ArrayList<Lunch> items, OnListFragmentInteractionListener listener) {
        mValues = items;
        mListener = listener;
        context = c;
        mLunches = new LunchData();
        mLunches.assignData(items);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_list, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        //mValues = mLunches.getAllLunches();
        //Lunch tmpLunch = mLunches.getAllLunches().get(position);
        holder.mItem = mLunches.getAllLunches().get(position);// mValues.get(position);
        holder.mNameView.setText(holder.mItem.getName());
        holder.mCaloriesView.setText("calories");
        new Calories(holder.mCaloriesView).execute(holder.mItem.getName());
        holder.mDateView.setText(holder.mItem.getDate());

        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != mListener) {
                    // Notify the active callbacks interface (the activity, if the
                    // fragment is attached to one) that an item has been selected.
                    //mListener.onListFragmentInteraction(holder.mItem);
                    //(())
                }
                //holder.mNameView.setText("CLICKED");
                ((MainActivity)context).selectedLunchId = holder.mItem.getId();
                ((MainActivity)context).addDetailsFragment(new LunchDetailsFragment(), "Lunch details");
                ((MainActivity)context).setViewPager(1, false);
            }
        });

    }

    @Override
    public int getItemCount() {
        //return mValues.size();
        return mLunches.getAllLunches().size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final TextView mNameView;
        public final TextView mCaloriesView;
        public final TextView mDateView;
        public Lunch mItem;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            mNameView = (TextView) view.findViewById(R.id.name_textView);
            mCaloriesView = (TextView) view.findViewById(R.id.calories_TextView);
            mDateView = (TextView) view.findViewById(R.id.date_textView);
        }

        @Override
        public String toString() {
            return super.toString() + " '" + mCaloriesView.getText() + "'";
        }
    }
}
