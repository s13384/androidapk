package com.pamo.art.lunchtracker;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;

import java.util.Date;

public class AddLunchActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_lunch);

        final EditText lunchNameED = (EditText) findViewById(R.id.addLunch_name_editText);
        final DatePicker datePicker = (DatePicker) findViewById(R.id.datePicker);
        Button addLunchBtn = (Button) findViewById(R.id.addLunch_Button);
        final Database db = new Database(this);

        addLunchBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Date date = new Date();
                String time = datePicker.getYear() + "-" + datePicker.getMonth() + "-" + datePicker.getDayOfMonth();
                time = time + " 00:00:00";
                //lunchNameED.setText(time);
                String name = lunchNameED.getText().toString();
                db.addLunch(name, time);
                Intent goBackToMainActivity = new Intent(getApplicationContext(), MainActivity.class);
                startActivity(goBackToMainActivity);
            }
        });
    }
}
