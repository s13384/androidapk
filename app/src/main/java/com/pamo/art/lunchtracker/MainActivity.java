package com.pamo.art.lunchtracker;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {


    private ViewPager mViewPager;
    private SectionsStatePagerAdapter mSectionsStatePagerAdapter;
    private LunchListFragment fragment = new LunchListFragment();
    private Database db;
    private LunchData lunchData;
    public int selectedLunchId;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        db = new Database(this);
        //db.addLunch("l", "2017-11-08");
        lunchData = new LunchData();
        lunchData.assignData(db.getAllLunches());
        selectedLunchId = db.getAllLunches().get(0).getId();
        ListView lunchListView = (ListView) findViewById(R.id.lunch_listView);
        FloatingActionButton addLunchBtn = (FloatingActionButton) findViewById(R.id.addLunchFAB);

        addLunchBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent addLunchIntent = new Intent(getApplicationContext(), AddLunchActivity.class);
                startActivity(addLunchIntent);
            }
        });

        BaseAdapter adapter = new LunchListViewAdapter(this, db.getAllLunches());
        lunchListView.setAdapter(adapter);

        lunchListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                TextView tv = (TextView) view.findViewById(R.id.id_textView);
                int id = Integer.parseInt(tv.getText().toString());
                //Intent intent = new Intent(getApplicationContext(), LunchDetailsActivity.class);
                //intent.putExtra("id", id);
                //startActivity(intent);
                setViewPager(1, false);
            }
        });

        mSectionsStatePagerAdapter = new SectionsStatePagerAdapter(getSupportFragmentManager());
        mViewPager = (ViewPager) findViewById(R.id.container);
        setupViewPager(mViewPager);
    }

    private void setupViewPager(ViewPager viewPager){
        SectionsStatePagerAdapter adapter = mSectionsStatePagerAdapter;
        adapter.addFragment(fragment, "Lunch List");
        adapter.addFragment(new LunchDetailsFragment(), "Lunch Details");
        adapter.addFragment(new LunchDetailsFragment(), "Lunch Details2");
        viewPager.setAdapter(adapter);
    }

    public void addDetailsFragment(Fragment fragment, String title){
        mSectionsStatePagerAdapter.forceFragmentAt1index(fragment,title);
        mSectionsStatePagerAdapter.addFragment(fragment, title);
        mViewPager.setAdapter(mSectionsStatePagerAdapter);
    }

    public void setViewPager(int fragmentNumber, boolean deleted){
        mSectionsStatePagerAdapter.notifyDataSetChanged();
        //mViewPager.setCurrentItem(fragmentNumber);
        if (deleted) {
            //mSectionsStatePagerAdapter.addFragment(fragment, "Lunch List");
            //mSectionsStatePagerAdapter.refresh();
            //mSectionsStatePagerAdapter.getItem(mViewPager.getCurrentItem()).onDestroy();
            //mSectionsStatePagerAdapter.notifyDataSetChanged();
            //mSectionsStatePagerAdapter.forceFragmentAt0index(fragment, "Lu");
            //mSectionsStatePagerAdapter.notifyDataSetChanged();
            //mViewPager.setCurrentItem(fragmentNumber);
            //mViewPager.setCurrentItem(0);
            /*
            mSectionsStatePagerAdapter = new SectionsStatePagerAdapter(getSupportFragmentManager());
            mSectionsStatePagerAdapter.addFragment(fragment, "Lunch List");
            mSectionsStatePagerAdapter.addFragment(new LunchDetailsFragment(), "Lunch Details");
            mSectionsStatePagerAdapter.addFragment(new LunchDetailsFragment(), "Lunch Details2");
            mViewPager.setAdapter(mSectionsStatePagerAdapter);*/
            setupViewPager(mViewPager);
        }
        else
        {
            mViewPager.setCurrentItem(fragmentNumber);
        }
        /*
        if (fragmentNumber == 0)
        {
            Fragment currentFragment = getFragmentManager().findFragmentById(R.id.list);
            if (currentFragment instanceof LunchFragmentListViewAdapter) {
                FragmentTransaction fragTransaction =   (getFragmentManager().beginTransaction();
                fragTransaction.detach(currentFragment);
                fragTransaction.attach(currentFragment);
                fragTransaction.commit();}
        }
        }
        */
    }
}
