package com.pamo.art.lunchtracker;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Locale;

/**
 * Created by Art on 2017-10-18.
 */

public class Lunch {


    private int id;
    private String name;
    private String date;
    private DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH-mm-ss", Locale.ENGLISH);

    Lunch(){};

    Lunch(int id, String name,String date){
        this.id = id;
        this.name = name;
        this.date = date;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
