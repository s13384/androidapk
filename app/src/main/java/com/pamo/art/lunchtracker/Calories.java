package com.pamo.art.lunchtracker;

import android.os.AsyncTask;
import android.widget.TextView;

import org.json.JSONObject;
import org.json.JSONTokener;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by Art on 2017-10-19.
 */

public class Calories extends AsyncTask<String, Void, String> {

    private TextView textView;

    Calories(TextView tv) {
        textView = tv;
    }

    @Override
    protected String doInBackground(String... strings) {
        //String foodName = strings[0];
        HttpURLConnection urlConnection = null;
        URL url = null;
        JSONObject object = null;
        InputStream inStream = null;
        String urlString = "http://szuflandia.pjwstk.edu.pl/~s13384/fakeCaloriesApi/index.php";
        try {
            url = new URL(urlString);
            urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setRequestMethod("GET");
            urlConnection.setDoOutput(true);
            urlConnection.setDoInput(true);
            urlConnection.connect();
            inStream = urlConnection.getInputStream();
            BufferedReader bReader = new BufferedReader(new InputStreamReader(inStream));
            String temp, response = "";
            while ((temp = bReader.readLine()) != null) {
                response += temp;
            }
            object = (JSONObject) new JSONTokener(response).nextValue();
            return object.getString("calories");
        } catch (Exception e) {
            //this.mException = e;
            return "error";
        }
    }

    @Override
    protected void onPostExecute(String result){
        textView.setText(result);
        super.onPostExecute(result);
    }
}
