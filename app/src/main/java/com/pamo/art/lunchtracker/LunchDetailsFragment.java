package com.pamo.art.lunchtracker;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link LunchDetailsFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link LunchDetailsFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class LunchDetailsFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    public LunchDetailsFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment LunchDetailsFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static LunchDetailsFragment newInstance(String param1, String param2) {
        LunchDetailsFragment fragment = new LunchDetailsFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_lunch_details, container, false);
        final Database db = new Database(getActivity());
        //final int id = 2; //getIntent().getExtras().getInt("id");
        //final Lunch lunch = db.getAllLunches().get(0);//db.getLunchWithId(id);
        final int id = ((MainActivity)getActivity()).selectedLunchId;
        final Lunch lunch = db.getLunchWithId(id);



        TextView nameTV = (TextView) view.findViewById(R.id.lunchDetail_name_textView);
        TextView dateTV = (TextView) view.findViewById(R.id.lunchDetail_date_textView);
        TextView caloriesTV = (TextView) view.findViewById(R.id.lunchDetail_calories_textView);
        if (lunch != null) {
            nameTV.setText(lunch.getName());
            dateTV.setText(lunch.getDate());
            caloriesTV.setText("calories");
            AsyncTask<String, Void, String> a = new Calories(caloriesTV).execute(lunch.getName());
        }
        else
        {

        }

        Button deleteLunchBtn = (Button) view.findViewById(R.id.delete_lunch_button);
        Button backBtn = (Button) view.findViewById(R.id.backToLunchListBtn);
        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((MainActivity)getActivity()).setViewPager(0, false);
            }
        });
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        builder.setTitle("Confirm");
        builder.setMessage("Are you sure?");
        builder.setPositiveButton("YES", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                // Do nothing but close the dialog
                db.deleteLunch(id);
                new LunchData().deleteLunch(id);
                Toast.makeText(getActivity(), "Deleted " + lunch.getName(), Toast.LENGTH_LONG).show();
                //Intent intent = new Intent(getActivity(), MainActivity.class);
                //startActivity(intent);
                //((MainActivity)getActivity()).setViewPager(0);
                dialog.dismiss();
                //((MainActivity)getActivity()).myNotify();
                ((MainActivity)getActivity()).setViewPager(0, true);
            }
        });
        builder.setNegativeButton("NO", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                // Do nothing
                dialog.dismiss();
            }
        });
        final AlertDialog alert = builder.create();

        deleteLunchBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alert.show();
                /*db.deleteLunch(id);
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                ((MainActivity)getActivity()).setViewPager(0);
                */
            }
        });
        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
