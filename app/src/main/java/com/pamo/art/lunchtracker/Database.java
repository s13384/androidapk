package com.pamo.art.lunchtracker;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;

/**
 * Created by Art on 2017-10-16.
 */

public class Database extends SQLiteOpenHelper {

    private SQLiteDatabase db;

    private final String TABLE_NAME = "lunch";
    private final String CREATE_TABLE_QUERY = "CREATE TABLE " + TABLE_NAME + " (id integer primary key autoincrement, name text,date text)";
    private final String SELECT_ALL_LUNCHES_QUERY = "SELECT id, name, date FROM " + TABLE_NAME;
    private final String INSERT_LUNCH_QUERY = "INSERT INTO";

    public Database(Context context){
        super(context, "nazwa_bazy.db", null, 1);
        db = getWritableDatabase();
        //addLunch("t","t");
    }
    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_TABLE_QUERY);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

    }

    public ArrayList<Lunch> getAllLunches(){
        ArrayList<Lunch> lunches = new ArrayList<Lunch>();
        Cursor result = db.rawQuery(SELECT_ALL_LUNCHES_QUERY, null);
        while (result.moveToNext()) {
            lunches.add(new Lunch(result.getInt(0), result.getString(1), result.getString(2)));
        }
        return lunches;
    }

    public void addLunch(String name, String date){
        ContentValues lunch = new ContentValues();
        lunch.put("name", name);
        lunch.put("date", date);
        db.insertOrThrow(TABLE_NAME, null, lunch);
    }

    public Lunch getLunchWithId(int id)
    {
        Lunch lunch;
        String[] columns = {"id", "name", "date"};
        String[] args = {id+""};
        Cursor resultCursor = db.query(TABLE_NAME, columns, "id=?", args, null, null, null);
        if(resultCursor.moveToNext())
        {
            lunch = new Lunch(resultCursor.getInt(0), resultCursor.getString(1), resultCursor.getString(2));
        }
        else lunch=null;
        return lunch;
    }

    public void deleteLunch(int lunchId){
        String ids[] = {lunchId+""};
        db.delete(TABLE_NAME, "id=?", ids);
    }
}
